#+TITLE: Org SSR

Server-side rendering of org files in the current directory.

Adapted from [[https://github.com/eschulte/emacs-web-server/blob/master/examples/007-org-mode-file-server.el][org-mode-file-server.el]].

* Installation (optional)

  First, [[https://gitlab.com/grinn.amy/org-ssr/-/releases][download the latest release]] (=org-ssr-x.y.z.el=)

  Then, =package-install-file RET org-ssr-x.y.z.el RET=

* Usage

** =org-ssr=
   The command =org-ssr= will create a file server in the current
   directory defined by =default-directory=. Only files in the
   immediate folder will be available over http. When called with a
   prefix argument, the current directory will be served recursively.

   Starting a server will list the available addresses in the
   minibuffer:

   #+begin_example
     Org ssr is serving folder /some/folder

     Available at:
       http://localhost:44259
       http://10.0.0.3:44259
   #+end_example

   Navigating to this address in the browser will list available file
   formats for all org files. If served recursively, all subdirectories
   will be listed as well.

   Requesting a new export format will kick off an asynchronous org
   export and serve the results. It's recommended to configure the
   variable =org-export-async-init-file= to point to an elisp file with
   the bare minimum config necessary to export org files.

   If a directory is requested, and that directory has an =index.org=
   file, that file will be sent as html rather than a directory
   listing.

   If a requested file or directory does not exist, and a =404.org=
   file does exist in the requested directory or any of its parent
   directories, that file will be sent in a format matching the
   requested one, falling back to html when necessary.

** =org-ssr-clear-cache=
   A cache of each export format will be saved to the hash table
   =org-ssr--cache=. Use the command =org-ssr-clear-cache= to reset the
   cache. Each cached export is automatically invalidated if the
   underlying org file is modified.
** As a docker image

   Docker tags:

   | tag            | description                    |
   |----------------+--------------------------------|
   | :latest, :main | The latest stable version      |
   | :x.y.z         | Tagged release                 |
   | :branch-name   | The latest build from a branch |

*** Publish port 8080 to an external port, eg 9000
    #+begin_src sh
      docker run -p 9000:8080 registry.gitlab.com/grinn.amy/org-ssr:latest
    #+end_src
*** Attach a volume to serve files
    #+begin_src sh
      docker run -v /path/to/serve:/public registry.gitlab.com/grinn.amy/org-ssr:latest
    #+end_src

*** Attach a volume for custom export conifg
    #+begin_src sh
      docker run -v /path/to/ox-config.el:/ox-config.el registry.gitlab.com/grinn.amy/org-ssr:latest
    #+end_src

*** Serve files recursively
    #+begin_src sh
      docker run -e RECURSIVE=t registry.gitlab.com/grinn.amy/org-ssr:latest
    #+end_src

*** Print debug messages to the console
    #+begin_src sh
      docker run -e VERBOSE=t registry.gitlab.com/grinn.amy/org-ssr:latest
    #+end_src
*** Run babel source blocks without confirmation
    Any #+CALL: properties in an org file will automatically
    be executed on export. Setting this is very unsafe!
    #+begin_src sh
      docker run -e BABEL=t registry.gitlab.com/grinn.amy/org-ssr:latest
    #+end_src
*** Combined example
    #+begin_src sh
      docker run \
             -p 9000:8080 \
             -e RECURSIVE=t \
             -e VERBOSE=t \
             -v /path/to/serve:/public \
             -v /path/to/ox-config.el:/ox-config.el \
             registry.gitlab.com/grinn.amy/org-ssr:latest
    #+end_src

*** docker-compose example
    #+begin_src yaml
      version: '3.9'
      services:
        org-ssr:
          image: registry.gitlab.com/grinn.amy/org-ssr:latest
          ports:
            - 9000:8080
          environment:
            - RECURSIVE=t
            - VERBOSE=t
          volumes:
            - ./public:/public
            - ./ox-config.el:/ox-config.el
    #+end_src

* Query parameters
  Query parameters can be accessed using the =org-ssr-query= function,
  which takes a symbol name of the query parameter, a default value,
  and a type (either ~'number~ or ~'string~).

  However, if using query parameters to change the output of the
  document (through babel scripts: =:babel t= or =(setq org-ssr-babel t)=),
  I recommend turning of the cache with the option =:no-cache t=
  or =(setq org-ssr-no-cache t)=, because the first request will 'lock
  in' the result for future requests, regardless of their query
  parameters.

  For example, to enable pagination, you could use a query parameter
  'page' with a default value of 0.

  #+begin_src org
    ,#+header: :var page=(org-ssr-query :page 0 'number)
    ,#+begin_src emacs-lisp :exports results :results value raw
      (format "* Page number %d" page)
    ,#+end_src
    
    ,#+RESULTS:
    ,* Page number 0
  #+end_src

  When someone requests =?page=2=, then the babel code block will
  produce:
  
  #+begin_src org
    ,* Page number 2
  #+end_src
