FROM silex/emacs:28.1-alpine

RUN apk add --no-cache coreutils

COPY org-ssr.el /root/.emacs.d/lisp/
COPY deps/web-server/web-server.el /root/.emacs.d/lisp/
COPY deps/web-server/web-server-status-codes.el /root/.emacs.d/lisp/
COPY deps/org-contrib/org-contrib.el /root/.emacs.d/lisp/
COPY deps/org-contrib/ox-rss.el /root/.emacs.d/lisp/

COPY serve /
RUN chmod +x /serve
COPY ox-config.el /
COPY index.org /public/

ENTRYPOINT /serve
